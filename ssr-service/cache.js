const NodeCache = require( "node-cache" );
const cache = new NodeCache();

function get(key){
    return new Promise((resolve, reject) => {
        cache.get(key, (err, res) => {
            if(err){
                reject(err);
            }
            resolve(res)
        })
    })
}

module.exports = () => {
    return async (req, res, next) => {
        let key = '__express__' + req.originalUrl || req.url
        let cachedBody = await get(key)
        if (cachedBody) {
            res.send(cachedBody)
            return
        } else {
            res.sendResponse = res.send
            res.send = (body) => {
                cache.set(key, body);
                res.sendResponse(body)
            }
            next()
        }
    }
}