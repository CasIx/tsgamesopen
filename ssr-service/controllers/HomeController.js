const router = require('express').Router();
const uuid = require('uuid/v1');
const game_service = require('../grpc/game-service');

var extract = require('extract-zip')

router.get('/', async (req, res) => {

    let searchCategories = [];

    req.query.category? searchCategories.push(req.query.category): searchCategories.push("all");


    game_service.List({
        skip : (req.query.page * 20) || 0,
        limit : 20,
        sortBy : req.query.sort || "name",
        categories: searchCategories
    }, (error, gamesT) => {
        
        if(error) {
            console.log(error);
            process.exit(1);
        }
        let games = gamesT.games

        
        game_service.GetCategories({}, (error, categories) => {


            

            let fullCategories = categories.categories.map(val => {

                console.log(`javascript:insertParam('category', '${val}');`)

                return {
                    name: val.name,
                    href: `javascript:insertParam('category', '${val.name}');`
                }
            })

            console.log(fullCategories)
            
            res.render("index.jade", {
            
                host: `${req.protocol}://${req.get('host')}/`,
                navItems: [{
                    type: "link",
                    text: "lil",
                    href: "#"      
                },
                {
                    type: "button",
                    onclick: "$('#AddGameModal').modal('show');",
                    class: 'btn-outline-success',
                    text: 'AddGame'
        
                },
                {
                    type: "button",
                    onclick: "$('#AddGameCategory').modal('show');",
                    class: 'btn-outline-success',
                    text: 'AddCategory'
        
                }
            ],
        
                pageHasSearch: true ,
        
                categories: fullCategories,
        
                curentPageIndex: req.query.page || 1,
                count: games.length,
                sortParams: ["name", "price"],
        
                games: games != []? games: false
        
                
            })
        })
    })
})

router.post('/', async (req, res) => {

    let fileId = uuid();

    await req.files.zip.mv(__dirname + '/../archives/' + fileId + req.files.zip.name);
    await req.files.image.mv(__dirname + '/../files/images/' + fileId)
    extract(__dirname + '/../archives/' + fileId + req.files.zip.name, {
        dir: __dirname + '/../files/' + fileId
    }, () => {})

    let categories = ["all"];

    req.body.category? categories.push(req.body.category): undefined;

    console.log({
        userId:'',
        fileId: fileId,
        name: req.body.name,
        price: parseInt(req.body.price),
        description: req.body.description,
        categories: categories
    })

    game_service.Create({
        userId:'',
        fileId: fileId,
        name: req.body.name,
        price: parseInt(req.body.price),
        description: req.body.description,
        categories: categories
    }, (error) => {
        console.log(error)
    })
}); 

router.post('/category', (req, res) => {

    console.log(req.body)

    game_service.AddCategory({
        name: req.body.name
    }, (error, _) => {
        if(error){
            console.log(error)
        }
    })
})

module.exports = router;