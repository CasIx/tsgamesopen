const router = require('express').Router();
const cache = require('../cache');
const user_service = require('../grpc/user-service')

const jwt = require('../jwt');

console.log(cache())

router.get('/register', cache(), (req, res) => {
    console.log("lol")
    res.render('register.jade', {
        host: `${req.protocol}://${req.get('host')}/`
    });
});

router.post('/register', (req, res) => {
    user_service.Register({
        email: req.body.email,
        name: req.body.name,
        password: req.body.password
    }, (err, res) => {
        if(err){
            throw new Error(err)
        }
        
    })
})

router.post('/login', (req, res) => {
    user_service.LogIn({
        login: req.body.login,
        password: req.body.password
    }, (error, result) => {
        
        if(result._id === ''){
            res.writeHead(302, {
                'Location': `${req.protocol}://${req.get('host')}/account/login`
            })
            res.end();
        }

        console.log(result._id)

        let token = jwt.sign(result._id);

        console.log(req.cookies.jwt);

        res.cookie("jwt", token, {
            maxAge: 3600000*24,
            httpOnly: true
        })

        console.log(token)

        res.writeHead(302, {
            'Location': `${req.protocol}://${req.get('host')}/`
        })

        res.end();

    })
})

router.get('/login', cache(),  (req, res) => {
    res.render('login.jade', {
        navItems: [{
            type: "link",
            text: "lil",
            href: "#"      
        }],
        pageHasSearch: false,
        host: `${req.protocol}://${req.get('host')}/`
    });
})

console.log(router)
module.exports = router;
