const express = require('express');
const app = express();
const bodyParser = require('body-parser')

const fileUpload = require('express-fileupload');

var cookieParser = require('cookie-parser')

app.use(fileUpload());
app.use(cookieParser());
app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(bodyParser.urlencoded())

require('dotenv').config();


const homeController = require('./controllers/HomeController');
const accountController = require('./controllers/AccountController');

const cache = require('./cache')

app.use('/files/games', express.static('./files'));
app.use('/static', cache(), express.static('./static'));

app.set('view engine', 'pug');

console.log(accountController)

app.use('/', homeController)
app.use('/account', accountController);

app.listen(parseInt(process.env.SERVER_PORT));