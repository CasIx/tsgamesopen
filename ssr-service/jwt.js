var jwt = require('jsonwebtoken');

module.exports = {
    sign : (payload) => {
        return jwt.sign(payload, process.env.JWT_KEY)
    },

    verify : (token) => {
        jwt.verify(token, process.env.JWT_KEY)
    }
}