let xhr = new XMLHttpRequest();



xhr.open('GET', 'http://localhost:3000/games', false);


xhr.send();

let games = JSON.parse(xhr.response);

games.forEach(game => {
    $('#games').append(`<div class="col-lg-4 col-md-6 mb-4">
                        <div class="card">
                            <a href="#"><img class="card-img-top" src="http://localhost:3000/source/games${game.image}" alt=""></a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="#">${game.name}</a>
                                </h4>
                                <h5>${game.price <= 0? 'Free': game.price + "$"}</h5>
                                <p class="card-text">${game.description}</p>
                            </div>
                            <div class="card-footer">
                                
                                <a href="http://localhost:3000/source/games/${game._id}/index.html" class="btn btn-outline-success w-100">Play</a>
                            </div>
                        </div>
                    </div>`)
});

