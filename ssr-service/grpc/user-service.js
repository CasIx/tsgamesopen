const grpc = require('grpc')

const protoPath = require('path').join(__dirname, '..','..', 'proto');
console.log(protoPath)
const proto = grpc.load({root: protoPath, file: 'UserService.proto'});
const client = new proto.users.UserService(`${process.env.USERSERVICE_HOST}:${process.env.USERSERVICE_PORT}`, grpc.credentials.createInsecure());

module.exports = client;