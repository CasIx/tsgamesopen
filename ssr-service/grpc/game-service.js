const grpc = require('grpc')

const protoPath = require('path').join(__dirname, '..','..', 'proto');

const proto = grpc.load({root: protoPath, file: 'GameService.proto'});


const client = new proto.games.GameService(`${process.env.GAMESERVICE_HOST}:${process.env.GAMESERVICE_PORT}`, grpc.credentials.createInsecure());

module.exports = client;