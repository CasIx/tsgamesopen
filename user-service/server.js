const UserServiceProtos = require('./UserService_pb')
const MongoClient = require('mongodb').MongoClient;
const CryptoJS = require("crypto-js");
const randomString = require('random-string')

require('dotenv').config();

const url = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}`;
const dbName = process.env.DB_NAME;
const collectionName = "users"

const grpc = require('grpc')
var PROTO_PATH = __dirname + '/../proto/UserService.proto';

var protoLoader = require('@grpc/proto-loader');

var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
var protoDescriptor = grpc.loadPackageDefinition(packageDefinition);



const route = protoDescriptor.users;

const Server = new grpc.Server();


MongoClient.connect(url, { useNewUrlParser: true })
    .then(client => {
        let db = client.db(dbName);
        let collection = db.collection(collectionName);
        
        Server.addService(route.UserService.service, {
            List: (call, callback) => {
                console.log(call)
                collection.find({}, {
                    _id: 1,
                    name: 1,
                    email: 1,
                })
                .skip(call.request.skip || 0)
                .limit(call.request.limit || 20)
                .sort(call.request.sortBy)
                .toArray()
                .then(res => {
                    callback(undefined, {
                        users: res
                    })
                })
                .catch(error => {
                    callback(error, undefined);
                })               
            },

            Register: (call, callback) => {

                let salt = randomString({
                    length: 20,
                    numeric: true,
                    letters: true,
                    special: true
                });

                let hashedPassword = CryptoJS.SHA3(`${call.request.password}${salt}`).toString()

                console.log({
                    name: call.request.name,
                    email: call.request.email,
                    salt: salt,
                    password: hashedPassword
                })

                collection.insertOne({
                    name: call.request.password,
                    email: call.request.email,
                    salt: salt,
                    password: hashedPassword
                })
                .then(() => {
                    callback(undefined, {})
                })
                .catch(error => {
                    callback(error, undefined);
                })
            },

            LogIn: (call, callback) => {
                collection.findOne({
                    $or:[
                        {email: call.request.login},
                        {name: call.request.login}
                    ]
                })
                .then(user => {
                    if(!user){
                        callback(undefined, {
                            _id: null
                        })
                    }

                    let password = CryptoJS.SHA3(`${call.request.password}${user.salt}`).toString();

                    if(user.password !== password){
                        callback(undefined, {
                            _id: null
                        })
                    }else{
                        callback(undefined, {
                            _id: user._id
                        })
                    }

                })
                .catch(error => {
                    callback(error, undefined)
                })
            } 
        })
         
        Server.bind(`0.0.0.0:${process.env.SERVER_PORT}`, grpc.ServerCredentials.createInsecure());
        Server.start();

    })



