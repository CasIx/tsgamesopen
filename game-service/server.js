var extract = require('extract-zip')
const fs = require('fs');
console.log(require('dotenv').config());

const MongoClient = require('mongodb').MongoClient;

const uuidv1 = require('uuid/v1');

const grpc = require('grpc')
var PROTO_PATH = __dirname + '/../proto/GameService.proto';

var protoLoader = require('@grpc/proto-loader');

var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
var protoDescriptor = grpc.loadPackageDefinition(packageDefinition);



const route = protoDescriptor.games;

const Server = new grpc.Server();


const url = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}`;
const dbName = process.env.DB_NAME;
const collectionName = "games"


MongoClient.connect(url, {
    useNewUrlParser: true
})
    .then(client => {
        let db = client.db(dbName);
        let collection = db.collection(collectionName);
        let categoryCollection = db.collection("categories")

        Server.addService(route.GameService.service,{
            Create: (call, callback) => {
                console.log(call)
                collection.insertOne(call.request)
                .catch(error => {
                    callback(error, undefined)
                })
            },
            List: async (call, callback) => {

                let sort = {}
                sort[call.request.sortBy] = 1
                
                console.log(call.request);
                
                collection.find({
                    categories: {
                        $in: call.request.categories
                    }
                })
                    .skip(call.request.skip || 0)
                    .limit(call.request.limit || 20)
                    .sort(sort)
                    .toArray()
                    .then(games => {
                        callback(undefined, {
                            games: games
                        })
                    })
                    .catch(error => {
                        callback(error, undefined)
                    })
                    
            },

            Get: (call, callback) => {
                collection.findOne(call.request._id)
                    .then(game => {
                        callback(undefined, game);
                    })
                    .catch(error => {
                        callback(error, undefined);
                    })
            },

            GetCategories: (call, callback) => {
                categoryCollection.find({}, {fields:{_id: 0}})
                    .toArray()     
                    .then(categories => {
                        console.log(categories)
                        callback(undefined, {
                            categories: categories
                        });
                    })
                    .catch( error => {
                        callback(error, undefined);
                    })
            },
            AddCategory: (call, callback) => {


                categoryCollection.insertOne(call.request)
                    .then(result => {
                        console.log(result)
                        callback(undefined, undefined)
                    })
                    .catch(error => {
                        console.log(error)
                        callback(error, undefined)
                    })
            }
        }),

        
            

        Server.bind(`0.0.0.0:${process.env.SERVER_PORT}`, grpc.ServerCredentials.createInsecure());
        Server.start();
    })